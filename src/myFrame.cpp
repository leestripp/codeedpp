#include <iostream>
#include <filesystem>

// wxWidgets
// UI
#include <wx/wx.h>
#include <wx/artprov.h>
#include <wx/stc/stc.h>
#include <wx/treelist.h>
#include <wx/splitter.h>
#include <wx/dataview.h>
#include <wx/button.h>
#include <wx/menuitem.h>
// files
#include <wx/stdpaths.h>
#include <wx/filename.h>
// config
#include <wx/persist/toplevel.h>

// codeED++
#include "myApp.h"
#include "myFrame.h"
#include "data/myClientData.h"
#include "autocomplete/lsAutoComplete.h"
// icons
#include "icons/icon32x32.xpm"

using namespace std;


wxBEGIN_EVENT_TABLE( myFrame, wxFrame )
	EVT_MENU( ID_Open_Project,	myFrame::OnOpenProject )
	EVT_MENU( ID_New_Project,	myFrame::OnNewProject )
	EVT_MENU( ID_File_Save, myFrame::OnFileSave )
	EVT_MENU( ID_File_Close, myFrame::OnFileClose )
	EVT_MENU( wxID_EXIT, myFrame::OnExit )
	// Edit
	EVT_MENU( ID_cut, myFrame::OnCut )
	EVT_MENU( ID_copy, myFrame::OnCopy )
	EVT_MENU( ID_paste, myFrame::OnPaste )
	EVT_MENU( ID_menu_find, myFrame::OnMenuFind )
	EVT_MENU( ID_find_next, myFrame::OnFindText )
	EVT_MENU( ID_menu_goto, myFrame::OnMenuGoto )

	// AutoComplete
	EVT_MENU( ID_AC_scan_folder, myFrame::OnAC_Scan_Folder )
	EVT_MENU( ID_AC_clear_all, myFrame::OnAC_Clear_All )

	// TreeListCtrl context menu
	EVT_TREELIST_ITEM_CONTEXT_MENU( ID_TreeList,  myFrame::OnContextMenu )
	EVT_MENU( ID_TLCM_refresh, myFrame::OnTLCM_Refresh )
	EVT_MENU( ID_TLCM_new_file, myFrame::OnTLCM_New_File )
	EVT_MENU( ID_TLCM_new_folder, myFrame::OnTLCM_New_Folder )
	EVT_MENU( ID_TLCM_open_file, myFrame::OnTLCM_Open_File )
	EVT_MENU( ID_TLCM_delete_file, myFrame::OnTLCM_Delete_File )
	EVT_MENU( ID_TLCM_delete_folder, myFrame::OnTLCM_Delete_Folder )
	EVT_MENU( ID_TLCM_scan_folder, myFrame::OnTLCM_Scan_Folder )

	EVT_TREELIST_ITEM_ACTIVATED( ID_TreeList, myFrame::onTreeItemActivated )
	EVT_STC_MARGINCLICK( ID_stc_editor, myFrame::OnMarginClick )
	EVT_STC_MODIFIED( ID_stc_editor, myFrame::OnTextModified )
	EVT_STC_CHARADDED( ID_stc_editor, myFrame::OnEditorCharAdded )

	EVT_BUTTON( ID_goto_button, myFrame::OnGotoLine )
	EVT_BUTTON( ID_find_button, myFrame::OnFindText )

	EVT_TEXT_ENTER( ID_findtext_enter, myFrame::OnFindText )
	EVT_SPINCTRL( ID_goto_changed, myFrame::OnGotoChanged )
wxEND_EVENT_TABLE()

// Implements MyApp& GetApp()
DECLARE_APP( myApp )

wxIMPLEMENT_APP( myApp );

// filesystem
namespace fs = std::filesystem;


myFrame::myFrame( const wxString& title, const wxPoint& pos, const wxSize& size ) : wxFrame(NULL, wxID_ANY, title, pos, size)
{
	m_currentPos = 0;
	m_findEOF = false;

    // Construct the image list with the standard images.
    InitImageList();

	SetIcon( icon32x32_xpm );
	
	// user dir
	wxFileName f( wxStandardPaths::Get().GetUserDataDir() );
	m_user_home = f.GetPath();

	// debug
	cout << "Users home : " <<  m_user_home << endl;

	m_autocomplete = new lsAutoComplete();
	if( m_autocomplete )
	{
		// TODO: make prefs to hold dirs to scan etc..
	}
	// Load some default.
	m_autocomplete->appendWordList( string(m_user_home.mb_str()) + "/.local/include" );
	// gtkmm 4
	m_autocomplete->appendWordList( "/usr/include/gtkmm-4.0" );
	
	// ******************
	// Menus
	// File
	wxMenu *menuFile = new wxMenu;
	menuFile->Append( ID_File_Save, "&Save File\tCtrl-S", "Save current file" );
	menuFile->Append( ID_File_Close, "&Close File\tCtrl-W", "Close current file" );
	menuFile->AppendSeparator();
	menuFile->Append( wxID_EXIT );
	// Edit
	wxMenu *menuEdit = new wxMenu;
	menuEdit->Append( ID_cut, "&Cut\tCtrl-X", "Cut selection" );
	menuEdit->Append( ID_copy, "&Copy\tCtrl-C", "Copy selection" );
	menuEdit->Append( ID_paste, "&Paste\tCtrl-V", "Paste selection" );
	menuFile->AppendSeparator();
	menuEdit->Append( ID_menu_find, "&Find\tCtrl-F", "Enter find text" );
	menuEdit->Append( ID_find_next, "&Find Next\tCtrl-G", "Find next text in documant" );
	menuEdit->Append( wxID_ANY, "&Find & Replace\tCtrl-R", "Replace text in document" );
	menuFile->AppendSeparator();
	menuEdit->Append( ID_menu_goto, "&Goto\tCtrl-J", "Enter find text" );
	// Project
	wxMenu *menuProject = new wxMenu;
	menuProject->Append( ID_Open_Project, "&Open Project\tCtrl-O", "Open project folder" );
	menuProject->Append( ID_New_Project, "&New Project\tCtrl-N", "Create new project" );
	// AutoComplete
	wxMenu *menuAutoComplete = new wxMenu;
	menuAutoComplete->Append( ID_AC_scan_folder, "&Scan Folder", "Scan folder for keywords" );
	menuAutoComplete->Append( ID_AC_clear_all, "Clear All", "Clear all keywords" );
	
	// Menubar
	wxMenuBar *menuBar = new wxMenuBar;
	menuBar->Append( menuFile, "&File" );
	menuBar->Append( menuEdit, "&Edit" );
	menuBar->Append( menuProject, "&Project" );
	menuBar->Append( menuAutoComplete, "&AutoComplete" );
	SetMenuBar( menuBar );
	
	// Window splitter
	wxBoxSizer *sizermain = new wxBoxSizer( wxVERTICAL );
	wxSplitterWindow *splittermain = new wxSplitterWindow( this, wxID_ANY );
	splittermain->SetSashGravity( 0 ); // 0.0 = only right side grows
	splittermain->SetMinimumPaneSize( 50 ); // Smalest size
	sizermain->Add( splittermain, 1,wxEXPAND, 0 );
	
	// Panels
	m_panel_left = new wxPanel( splittermain, wxID_ANY );
	m_panel_right = new wxPanel( splittermain, wxID_ANY );
	
	// Treelistctrl
	wxBoxSizer *tree_sizer = new wxBoxSizer(wxVERTICAL);
	m_TreeListCtrl = new wxTreeListCtrl( m_panel_left, ID_TreeList, wxPoint(0,0), wxSize(200,300), wxTL_SINGLE | wxTL_DEFAULT_STYLE );
	m_TreeListCtrl->SetImageList( m_imageList );
	tree_sizer->Add( m_TreeListCtrl, 1, wxEXPAND, 0 );
	m_panel_left->SetSizer( tree_sizer );

	// int 	AppendColumn( const wxString &title, int width=wxCOL_WIDTH_AUTOSIZE, wxAlignment align=wxALIGN_LEFT, int flags=wxCOL_RESIZABLE)
	m_TreeListCtrl->AppendColumn( "Item", wxCOL_WIDTH_AUTOSIZE, wxALIGN_LEFT, wxCOL_SORTABLE | wxCOL_RESIZABLE );
	m_TreeListCtrl->AppendColumn( "Type", wxCOL_WIDTH_AUTOSIZE, wxALIGN_LEFT, wxCOL_SORTABLE | wxCOL_RESIZABLE );

	// right sizer
	wxBoxSizer *right_sizer = new wxBoxSizer( wxVERTICAL );

	// ******************
	// find and Goto
	wxPanel *find_panel = new wxPanel( m_panel_right, wxID_ANY, wxDefaultPosition, wxDefaultSize );
	wxBoxSizer *find_sizer = new wxBoxSizer( wxHORIZONTAL );

	// Goto
	wxStaticText *label_goto = new wxStaticText( find_panel, wxID_ANY, "Goto : " );
	m_gotoVal = new wxSpinCtrl( find_panel, ID_goto_changed, "", wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	m_gotoVal->SetRange( 1, 10000000 );
	wxButton *goto_button = new wxButton( find_panel, ID_goto_button, "GO", wxDefaultPosition, wxDefaultSize );
	
	// Find
	wxStaticText *label_find = new wxStaticText( find_panel, wxID_ANY, "Find : " );
	m_findText = new wxTextCtrl( find_panel, ID_findtext_enter, "", wxDefaultPosition, wxSize(200,-1), wxTE_PROCESS_ENTER );
	wxButton *find_button = new wxButton( find_panel, ID_find_button, "Find", wxDefaultPosition, wxDefaultSize );
	
	find_sizer->Add( label_goto, 0, wxEXPAND | wxHORIZONTAL | wxALL, 5 );
	find_sizer->Add( m_gotoVal, 0, wxEXPAND | wxHORIZONTAL | wxALL, 5 );
	find_sizer->Add( goto_button, 0, wxEXPAND | wxHORIZONTAL | wxALL, 5 );
	 find_sizer->AddStretchSpacer( 1 );
	find_sizer->Add( label_find, 0, wxALL, 5 );
	find_sizer->Add( m_findText, 0, wxALL, 5 );
	find_sizer->Add( find_button, 0, wxALL, 5 );
	find_panel->SetSizer( find_sizer );

	right_sizer->Add( find_panel, 0, wxEXPAND | wxHORIZONTAL );

	// create wxStyledTextCtrl
	if( setup_Editor() )
	{
		right_sizer->Add( m_editor, 1, wxEXPAND | wxALL );
	}
	m_panel_right->SetSizer( right_sizer );

	// Add panels
	splittermain->SplitVertically( m_panel_left, m_panel_right );

	// Add to main frame
	this->SetSizer( sizermain );
	sizermain->SetSizeHints( this );

	// Status bar
    CreateStatusBar();

	// Restore the previously saved geometry, if any, and register this frame
	// for its geometry to be saved when it is closed using the given wxConfig
	// key name.
	if(! wxPersistentRegisterAndRestore( this, "com.leestripp.codeED++") )
	{
		// Choose some custom default size for the first run -- or don't do
		// anything at all and let the system use the default initial size.
		SetClientSize( FromDIP(wxSize(800, 800)) );
	}
}

myFrame::~myFrame()
{
	if( m_imageList ) delete m_imageList;
	if( m_autocomplete ) delete m_autocomplete;
}

void myFrame::InitImageList()
{
	wxSize iconSize = wxArtProvider::GetSizeHint(wxART_LIST, this);
	if ( iconSize == wxDefaultSize ) iconSize = FromDIP(wxSize(16, 16));

	m_imageList = new wxImageList( iconSize.x, iconSize.y );

	// The order should be the same as for the enum elements.
	static const wxString icons[] = {
		wxART_NORMAL_FILE,
		wxART_FOLDER,
		wxART_FOLDER_OPEN,
		wxART_DELETE
	};

    for( unsigned n = 0; n < WXSIZEOF(icons); n++ )
    {
        m_imageList->Add( wxArtProvider::GetIcon(icons[n], wxART_LIST, iconSize) );
    }
}


void myFrame::OnOpenProject( wxCommandEvent& event )
{
	wxDirDialog Dir( NULL, "Choose Project Folder", "", wxDD_DEFAULT_STYLE | wxDD_CHANGE_DIR );

	int result = Dir.ShowModal();
	if( result == wxID_OK )
	{
		// Clear TreeListctrl
		m_TreeListCtrl->DeleteAllItems();
		// Clear Editor
		m_editor->ClearAll();

		m_projectDir = Dir.GetPath().ToStdString();
		cout << "Open project : " << m_projectDir << endl;
		
		// Load Project Folders
		wxTreeListItem root = m_TreeListCtrl->GetRootItem();
		loadTree( m_projectDir, 0, root );
		m_TreeListCtrl->Expand( m_TreeListCtrl->GetFirstChild( root) );

		// Sort by Name Col 1
		m_TreeListCtrl->SetSortColumn( 0, true );
		m_TreeListCtrl->GetDataView()->GetModel()->Resort();
	}
}

void myFrame::OnNewProject( wxCommandEvent& event )
{
	wxDirDialog Dir( NULL, "Choose New Project Folder", "", wxDD_DEFAULT_STYLE | wxDD_CHANGE_DIR );
	
	int result = Dir.ShowModal();
	if( result == wxID_OK )
	{
		// Clear TreeListctrl
		m_TreeListCtrl->DeleteAllItems();
		// Clear Editor
		m_editor->ClearAll();

		m_projectDir = Dir.GetPath().ToStdString();
		cout << "Create project : " << m_projectDir << endl;
		
		//	Create build dir
		if(! fs::exists( m_projectDir + "/build" ) )
		{
			fs::create_directory( m_projectDir + "/build" );
		}
		
		//	Create tests dir
		if(! fs::exists( m_projectDir + "/tests" ) )
		{
			fs::create_directory( m_projectDir + "/tests" );
			
			// Add cmake file.
			stringstream ss;
			ss << "# global" << endl;
			ss << endl;
			ss << "project(tests LANGUAGES CXX)" << endl;
			ss << endl;
			ss << "include_directories(" << endl;
			ss << ")" << endl;
			ss << endl;
			ss << "# test1" << endl;
			ss << "add_executable(test1" << endl;
			ss << "\ttest1.cpp" << endl;
			ss << ")" << endl;
			ss << "target_link_libraries(test1" << endl;
			ss << ")" << endl;
			ss << "add_dependencies(test1" << endl;
			ss << ")" << endl;
			
			ofstream cmakefile( m_projectDir + "/tests/CMakeLists.txt" );
			cmakefile << ss.str() << endl;
			cmakefile.close();
			
			// create test1.cpp
			ss.str( string() );
			ss << "#include <iostream>" << endl;
			ss << endl;
			ss << "int main()" << endl;
			ss << "{" << endl;
			ss << "\treturn 0;" << endl;
			ss << "}" << endl;
			ss << endl;
			
			ofstream test1file( m_projectDir + "/tests/test1.cpp" );
			test1file << ss.str() << endl;
			test1file.close();
		}
		
		//	Create CMakeLists.txt
		if(! fs::exists( m_projectDir + "/CMakeLists.txt" ) )
		{
			stringstream ss;
			ss << "cmake_minimum_required(VERSION 3.28)" << endl;
			ss << endl;
			ss << "project(myProject LANGUAGES CXX)" << endl;
			ss << endl;
			ss << "if(NOT CMAKE_BUILD_TYPE)" << endl;
			ss << "\tset(CMAKE_BUILD_TYPE Release)" << endl;
			ss << "endif()" << endl;
			ss << "set(CMAKE_CXX_FLAGS \"-Wall -Wextra\")" << endl;
			ss << "set(CMAKE_CXX_FLAGS_DEBUG \"-g\")" << endl;
			ss << "set(CMAKE_CXX_FLAGS_RELEASE \"-O3\")" << endl;
			ss << "set(CMAKE_CXX_STANDARD 20)" << endl;
			ss << "set(CMAKE_CXX_STANDARD_REQUIRED ON)" << endl;
			ss << "# Install to users home dir." << endl;
			ss << "set(CMAKE_INSTALL_PREFIX \"$ENV{HOME}/.local\")" << endl;
			ss << endl;
			ss << "add_subdirectory(myProject)" << endl;
			ss << "if(NOT NO_TESTS)" << endl;
			ss << "\tadd_subdirectory(tests)" << endl;
			ss << "endif()" << endl;
			
			ofstream cmakefile( m_projectDir + "/CMakeLists.txt" );
			cmakefile << ss.str() << endl;
			cmakefile.close();
		}
		
		//	Create Readmee.md
		if(! fs::exists( m_projectDir + "/Readme.md" ) )
		{
			ofstream readmefile( m_projectDir + "/Readme.md" );
			readmefile << "## myProject v0.1-Alpha" << std::endl;
			readmefile.close();
		}
		
		//	Create .gitignore
		if(! fs::exists( m_projectDir + "/.gitignore" ) )
		{
			ofstream gitfile( m_projectDir + "/.gitignore" );
			gitfile << "# git" << std::endl;
			gitfile << "build" << std::endl;
			gitfile << "secrets" << std::endl;
			gitfile.close();
		}

		// Load Project Folders
		wxTreeListItem root = m_TreeListCtrl->GetRootItem();
		loadTree( m_projectDir, 0, root );
		m_TreeListCtrl->Expand( m_TreeListCtrl->GetFirstChild( root) );
		
		// Sort by File Col 1
		m_TreeListCtrl->SetSortColumn( 0, true );
		m_TreeListCtrl->GetDataView()->GetModel()->Resort();
		
		// Clear autocomplete
		m_autocomplete->clearWordList();
	}
}

void myFrame::OnFileSave( wxCommandEvent& event )
{
	// debug
	// cout << "OnFileSave called..." << endl;
	saveCurrentFile();
}

void myFrame::OnFileClose( wxCommandEvent& event )
{
	// debug
	// cout << "OnFileClose called..." << endl;
	saveCurrentFile();

	m_currentFile = "";
	m_editor->ClearAll();
}

void myFrame::saveCurrentFile()
{
	if( m_currentFile == "" )
	{
		// TODO: Save As...
		// Do we need this?
		
	} else
	{
		// grab out carret position
		m_currentPos = m_editor->GetCurrentPos();
		
		// Save...
		m_editor->SaveFile( m_currentFile );
		
		// save carret pos
		wxTreeListItem item = findTreeItem( m_currentFile );
		if( item.IsOk() )
		{
			myClientData *data = (myClientData *)m_TreeListCtrl->GetItemData( item );
			if( data )
			{
				data->StoreCarretPos( m_currentPos );
			}
		}
	}
}

void myFrame::OnExit(wxCommandEvent& event)
{
	Close( true );
}

void myFrame::onTreeItemActivated( wxTreeListEvent& event )
{
	wxTreeListItem item = event.GetItem();

	myClientData *data = (myClientData *)m_TreeListCtrl->GetItemData( item );
	if( data )
	{
		// debug
		// cout << "Item: " << m_TreeListCtrl->GetItemText( item );
		// cout << "  Full Path: " << data->GetFullPath() << endl;
		
		if(    data->GetItemType() == myClientData::SourceCpp
			|| data->GetItemType() == myClientData::SourceHeader
			|| data->GetItemType() == myClientData::SourceC
			|| data->GetItemType() == myClientData::TextFile
			|| data->GetItemType() == myClientData::Git
			|| data->GetItemType() == myClientData::Markdown
			|| data->GetItemType() == myClientData::XPM
			)
		{
			// auto save current file first
			saveCurrentFile();
			
			// load our file
			m_currentFile = data->GetFullPath();
			m_editor->LoadFile( m_currentFile );
			
			// move to stored carret pos
			m_editor->SetFocus();
			m_editor->GotoPos( data->GetCarretPos() );
			// debug
    		// cout << "Goto carret Pos : " << data->GetCarretPos() << endl;
			
			// Status bar
			SetStatusText( "Curent File : " + m_currentFile, 0 );
		} else
		{
			// dir sotoggle expand
			if( m_TreeListCtrl->IsExpanded( item ) )
			{
				m_TreeListCtrl->Collapse( item );
			} else
			{
				m_TreeListCtrl->Expand( item );
			}
		}
	}
}

bool myFrame::setup_Editor()
{
	// Add wxStyleTextCtrl
	m_editor = new wxStyledTextCtrl( m_panel_right, ID_stc_editor );
	if(! m_editor )
	{
		cerr << "ERROR: Could not create wxStyledTextCtrl" << endl;
		return false;
	}

	// Style
	m_editor->StyleClearAll();
	m_editor->SetLexer( wxSTC_LEX_CPP );

	// Tabs
	m_editor->SetUseTabs( true );
	m_editor->SetTabWidth( 4 );

	// default spec
	// SPEC: bold turns on bold italic turns on italics fore:[name or #RRGGBB] sets the foreground colour
	// back:[name or #RRGGBB] sets the background colour face:[facename] sets the font face name to use
	// size:[num] sets the font size in points eol turns on eol filling underline turns on underlining
	wxString spec = "fore:#ffffff,back:#0f0f0f,face:SourceCode Pro,size:12";
	// 256 default styles
	for( int i=0; i<255; i++ )
	{
		m_editor->StyleSetSpec( i, spec );
	}
	
	// Caret
	m_editor->SetCaretLineVisible( true );
	m_editor->SetCaretLineBackground( wxColour(0,0,0) );



	// Line numbers
	m_editor->SetMarginWidth( MARGIN_LINE_NUMBERS, 50 );
	m_editor->StyleSetForeground( wxSTC_STYLE_LINENUMBER, wxColour( 220, 220, 220) );
	m_editor->StyleSetBackground( wxSTC_STYLE_LINENUMBER, wxColour( 30, 30, 30 ) );
	m_editor->SetMarginType( MARGIN_LINE_NUMBERS, wxSTC_MARGIN_NUMBER );
	
	// code folding
	m_editor->SetMarginType( MARGIN_FOLD, wxSTC_MARGIN_SYMBOL );
	m_editor->SetMarginWidth( MARGIN_FOLD, 15 );
	m_editor->SetMarginMask( MARGIN_FOLD, wxSTC_MASK_FOLDERS );
	m_editor->StyleSetBackground( MARGIN_FOLD, wxColor( 20, 20, 20 ) );
	m_editor->SetMarginSensitive( MARGIN_FOLD, true );

	wxColor grey( 150, 150, 150 );
	m_editor->MarkerDefine( wxSTC_MARKNUM_FOLDER, wxSTC_MARK_ARROW );
	m_editor->MarkerSetForeground( wxSTC_MARKNUM_FOLDER, grey );
	m_editor->MarkerSetBackground( wxSTC_MARKNUM_FOLDER, wxColour( 30, 30, 30 ) );

	m_editor->MarkerDefine (wxSTC_MARKNUM_FOLDEROPEN, wxSTC_MARK_ARROWDOWN);
	m_editor->MarkerSetForeground (wxSTC_MARKNUM_FOLDEROPEN, grey);
	m_editor->MarkerSetBackground (wxSTC_MARKNUM_FOLDEROPEN, wxColour( 30, 30, 30 ));
	
	m_editor->MarkerDefine (wxSTC_MARKNUM_FOLDERSUB, wxSTC_MARK_EMPTY);
	m_editor->MarkerSetForeground (wxSTC_MARKNUM_FOLDERSUB, grey);
	m_editor->MarkerSetBackground (wxSTC_MARKNUM_FOLDERSUB, wxColour( 30, 30, 30 ));
	
	m_editor->MarkerDefine (wxSTC_MARKNUM_FOLDEREND, wxSTC_MARK_ARROW);
	m_editor->MarkerSetForeground (wxSTC_MARKNUM_FOLDEREND, grey);
	m_editor->MarkerSetBackground (wxSTC_MARKNUM_FOLDEREND, wxColour( 30, 30, 30 ));

	m_editor->MarkerDefine (wxSTC_MARKNUM_FOLDEROPENMID, wxSTC_MARK_ARROWDOWN);
	m_editor->MarkerSetForeground (wxSTC_MARKNUM_FOLDEROPENMID, grey);
	m_editor->MarkerSetBackground (wxSTC_MARKNUM_FOLDEROPENMID, wxColour( 30, 30, 30 ));

	m_editor->MarkerDefine (wxSTC_MARKNUM_FOLDERMIDTAIL, wxSTC_MARK_EMPTY);
	m_editor->MarkerSetForeground (wxSTC_MARKNUM_FOLDERMIDTAIL, grey);
	m_editor->MarkerSetBackground (wxSTC_MARKNUM_FOLDERMIDTAIL, wxColour( 30, 30, 30 ));
	
	m_editor->MarkerDefine (wxSTC_MARKNUM_FOLDERTAIL, wxSTC_MARK_EMPTY);
	m_editor->MarkerSetForeground (wxSTC_MARKNUM_FOLDERTAIL, grey);
	m_editor->MarkerSetBackground (wxSTC_MARKNUM_FOLDERTAIL, wxColour( 30, 30, 30 ));
	// End of code folding part
	
	// Properties found from http://www.scintilla.org/SciTEDoc.html
	m_editor->SetProperty( "fold",        "1" );
	m_editor->SetProperty( "fold.comment", "1" );
	m_editor->SetProperty( "fold.compact", "1" );

	// word wrap
	m_editor->SetWrapMode( wxSTC_WRAP_WORD );

	m_editor->SetBackgroundColour( wxColor( 10, 10, 10 ) );
	
	m_editor->StyleSetForeground (wxSTC_C_STRING,					wxColour( 220, 200, 120 ) );
	m_editor->StyleSetForeground (wxSTC_C_PREPROCESSOR,				wxColour( 200,145, 100 ) );
	m_editor->StyleSetForeground (wxSTC_C_IDENTIFIER,				wxColour( 220, 120, 255 ) );
	m_editor->StyleSetForeground (wxSTC_C_NUMBER,					wxColour( 200, 200, 120 ));
	m_editor->StyleSetForeground (wxSTC_C_CHARACTER,				wxColour( 200, 200, 180 ) );
	m_editor->StyleSetForeground (wxSTC_C_WORD,						wxColour( 150, 150, 220 ) );
	m_editor->StyleSetForeground (wxSTC_C_WORD2,					wxColour( 120, 240, 120 ) );
	m_editor->StyleSetForeground (wxSTC_C_COMMENT,					wxColour( 160, 160, 150 ) );
	m_editor->StyleSetForeground (wxSTC_C_COMMENTLINE,				wxColour( 220, 220, 200 ) );
	m_editor->StyleSetForeground (wxSTC_C_COMMENTDOC,				wxColour( 150, 150, 150 ) );
	m_editor->StyleSetForeground (wxSTC_C_COMMENTDOCKEYWORD,		wxColour( 100, 100,200 ) );
	m_editor->StyleSetForeground (wxSTC_C_COMMENTDOCKEYWORDERROR,	wxColour( 100, 100, 220 ) );
	m_editor->StyleSetBold(wxSTC_C_WORD, true);
	m_editor->StyleSetBold(wxSTC_C_WORD2, true);
	m_editor->StyleSetBold(wxSTC_C_COMMENTDOCKEYWORD, true);

	// Selections
	m_editor->SetSelForeground( true, wxColor( 10, 10, 10 ) );
	m_editor->SetSelBackground( true, wxColor( 220, 220, 100 ) );

	// Carrot (Cursor)
	m_editor->SetCaretForeground( wxColor( 200, 200, 200 ) );

	// A sample list of keywords
	m_editor->SetKeyWords(0, wxT("class return for while switch break continue if else"));
	m_editor->SetKeyWords(1, wxT("const int float void char double"));

	return true;
}

// *****************
// AutoComplete
// *****************

void myFrame::scanFolder( const std::string &path )
{
	// does path exist
	if( fs::is_directory( path ) )
	{
		m_autocomplete->appendWordList( path );
	} else return;
}

// *****************
// TreeList
// *****************

void myFrame::loadTree( const std::string &path, int lvl, wxTreeListItem node )
{
	if( lvl == 0 )
	{
		// does path exist
		if(! fs::is_directory( path ) )
		{
			m_projectDir = "";
			return;
		}

		// AppendItem (wxTreeListItem parent, const wxString &text, int imageClosed=NO_IMAGE, int imageOpened=NO_IMAGE, wxClientData *data=NULL)
		wxTreeListItem project = m_TreeListCtrl->AppendItem( node, path, wxTreeListCtrl::NO_IMAGE, wxTreeListCtrl::NO_IMAGE, new myClientData( myClientData::Root, path ) );
		m_TreeListCtrl->SetItemText( project, 1, "Project" );
		node = project;
	}

	lvl++;
	for( const auto& entry : fs::directory_iterator( path ) )
	{
		const auto dirStr = entry.path().string();
		const auto filenameStr = entry.path().filename().string();
		const auto extStr = entry.path().extension().string();
		
		// debug
		// cout << "lvl: " << lvl << "  path: " << dirStr << "  file: " << filenameStr << endl;
		
		if( entry.is_directory() )
		{
			// Skip some folders : .git build etc.
			if( filenameStr == "build" || filenameStr == ".git" )
			{
				// skip dir
			} else
			{
				wxTreeListItem new_node = m_TreeListCtrl->AppendItem( node, filenameStr, myFrame::Icon_FolderClosed, myFrame::Icon_FolderOpened, new myClientData( myClientData::Folder, dirStr ) );
				m_TreeListCtrl->SetItemText( new_node, 1, "Directory" );
				// recursive
				loadTree( dirStr, lvl, new_node );
			}
			
		} else if( entry.is_regular_file() )
		{
			// Types
			// Root, TextFile, SourceCpp,  SourceHeader, Image, Git, Folder, Unknown
			wxTreeListItem tmp;
			
			if( filenameStr == "CMakeLists.txt" )
			{
				tmp = m_TreeListCtrl->AppendItem( node, filenameStr, myFrame::Icon_File, wxTreeListCtrl::NO_IMAGE, new myClientData( myClientData::TextFile, dirStr ) );
				m_TreeListCtrl->SetItemText( tmp, 1, "CMake" );
			} else if( filenameStr == ".gitignore" )
			{
				tmp = m_TreeListCtrl->AppendItem( node, filenameStr, myFrame::Icon_File, wxTreeListCtrl::NO_IMAGE, new myClientData( myClientData::Git, dirStr ) );
				m_TreeListCtrl->SetItemText( tmp, 1, "Git" );
			} else if( extStr == ".cpp" || extStr == ".cc" || extStr == ".c++" )
			{
				tmp = m_TreeListCtrl->AppendItem( node, filenameStr, myFrame::Icon_File, wxTreeListCtrl::NO_IMAGE, new myClientData( myClientData::SourceCpp, dirStr ) );
				m_TreeListCtrl->SetItemText( tmp, 1, "cpp" );
			} else if( extStr == ".c" )
			{
				tmp = m_TreeListCtrl->AppendItem( node, filenameStr, myFrame::Icon_File, wxTreeListCtrl::NO_IMAGE, new myClientData( myClientData::SourceC, dirStr ) );
				m_TreeListCtrl->SetItemText( tmp, 1, "C" );
			} else if( extStr == ".h" ||  extStr == ".hpp" )
			{
				tmp = m_TreeListCtrl->AppendItem( node, filenameStr, myFrame::Icon_File, wxTreeListCtrl::NO_IMAGE, new myClientData( myClientData::SourceHeader, dirStr ) );
				m_TreeListCtrl->SetItemText( tmp, 1, "header" );
			} else if( extStr == ".png" || extStr == ".jpg" || extStr == ".exr" )
			{
				tmp = m_TreeListCtrl->AppendItem( node, filenameStr, myFrame::Icon_File, wxTreeListCtrl::NO_IMAGE, new myClientData( myClientData::Image, dirStr ) );
				m_TreeListCtrl->SetItemText( tmp, 1, "Image" );
			} else if( extStr == ".md" )
			{
				tmp = m_TreeListCtrl->AppendItem( node, filenameStr, myFrame::Icon_File, wxTreeListCtrl::NO_IMAGE, new myClientData( myClientData::Markdown, dirStr ) );
				m_TreeListCtrl->SetItemText( tmp, 1, "Markdown" );
			} else if( extStr == ".xpm" )
			{
				tmp = m_TreeListCtrl->AppendItem( node, filenameStr, myFrame::Icon_File, wxTreeListCtrl::NO_IMAGE, new myClientData( myClientData::XPM, dirStr ) );
				m_TreeListCtrl->SetItemText( tmp, 1, "Image C Code" );
			} else if( extStr == ".desktop" )
			{
				tmp = m_TreeListCtrl->AppendItem( node, filenameStr, myFrame::Icon_File, wxTreeListCtrl::NO_IMAGE, new myClientData( myClientData::TextFile, dirStr ) );
				m_TreeListCtrl->SetItemText( tmp, 1, "Linux Launcher" );
			} else if( extStr == ".css" )
			{
				tmp = m_TreeListCtrl->AppendItem( node, filenameStr, myFrame::Icon_File, wxTreeListCtrl::NO_IMAGE, new myClientData( myClientData::TextFile, dirStr ) );
				m_TreeListCtrl->SetItemText( tmp, 1, "Style Sheet" );
			} else if( extStr == ".sh" )
			{
				tmp = m_TreeListCtrl->AppendItem( node, filenameStr, myFrame::Icon_File, wxTreeListCtrl::NO_IMAGE, new myClientData( myClientData::TextFile, dirStr ) );
				m_TreeListCtrl->SetItemText( tmp, 1, "Shell Script" );
			} else
			{
				tmp = m_TreeListCtrl->AppendItem( node, filenameStr, myFrame::Icon_File, wxTreeListCtrl::NO_IMAGE, new myClientData( myClientData::Unknown, dirStr ) );
				m_TreeListCtrl->SetItemText( tmp, 1, "File" );
			}
			
		} else
		{
			cout << "ERROR: unknown directory entry - " << filenameStr << endl;
		}
	}
}

wxTreeListItem myFrame::findTreeItem( const string& s )
{
	wxTreeListItem found_item;
	
	for( wxTreeListItem item = m_TreeListCtrl->GetFirstItem() ; item.IsOk() ; item = m_TreeListCtrl->GetNextItem(item) )
	{
		myClientData *data = (myClientData *)m_TreeListCtrl->GetItemData( item );
		if( data )
		{
			if( data->GetFullPath() == s )
			{
				// debug
				// cout << "Item found : " << s << endl;
				found_item = item;
				break;
			}
		}
	}
	return found_item;
}


/** Event callback when a margin is clicked, used here for code folding */
void myFrame::OnMarginClick( wxStyledTextEvent &event )
{
	if( event.GetMargin() == MARGIN_FOLD )
	{
		int lineClick = m_editor->LineFromPosition(event.GetPosition() );
		int levelClick = m_editor->GetFoldLevel(lineClick );

		if( ( levelClick & wxSTC_FOLDLEVELHEADERFLAG ) > 0 )
		{
			m_editor->ToggleFold( lineClick );
		}
	}
}


void myFrame::OnTextModified( wxStyledTextEvent& event )
{
	if(! m_autocomplete ) return;
	
   	// debug -- BUG: called many times.
	// cout << "myFrame::OnTextModified - Called" << endl;
	
	string prefix;
	
	m_currentPos = m_editor->GetCurrentPos();
    int wordStartPos = m_editor->WordStartPosition( m_currentPos, true );
	prefix = m_editor->GetRange( wordStartPos, m_currentPos ).ToStdString();
	// Event is called multiple times, this is a workaround.
	if( m_prefix == prefix )
	{
		return;
	}
	m_prefix = prefix; // Store new prefix
	
	// Display the autocompletion list
	int lenEntered = m_currentPos - wordStartPos;
	if( lenEntered > 0 )
    {
    	// wxStyledTextCtrl crashes if words is empty.
    	string words = m_autocomplete->getWords( m_prefix );
    	if( words != "" )
    	{
    		// debug
			// cout << "Word prefix: " <<  m_prefix << endl;
			m_editor->AutoCompShow( lenEntered, words );
    	}
	}
}


// ********************************
// Check what char was inserted.

void myFrame::OnEditorCharAdded( wxStyledTextEvent& event )
{
	int keycode = event.GetKey();

	// debug
	// cout << "Keycode : " << keycode << endl;

	switch( keycode )
	{
		case 10:		// Enter key
		{
			// AUTO indent
			int line = m_editor->GetCurrentLine();
			int ind = m_editor->GetLineIndentation( line - 1 );
			m_editor->SetLineIndentation( line, ind );
			int lineend = m_editor->GetLineEndPosition( line);
			m_editor->GotoPos( lineend );
		}
			break;

		default:
			break;
	}
}


void myFrame::OnGotoLine( wxCommandEvent& event )
{
	int val = m_gotoVal->GetValue();
	val--; // match 0 base
	m_editor->GotoLine( val );
	m_editor->SetFocus();

	// debug
	// cout << "Goto value : " << val << endl;

}

void myFrame::OnFindText( wxCommandEvent& event )
{
	wxString val = m_findText->GetValue();
	
	/*
	Flags
		wxSTC_FIND_CXX11REGEX
		wxSTC_FIND_REGEXP
		wxSTC_FIND_POSIX
		wxSTC_FIND_MATCHCASE
		wxSTC_FIND_WORDSTART
		wxSTC_FIND_WHOLEWORD
	*/

	int line = 0;
	if( m_findEOF )
	{
		m_editor->GotoLine( line );
		m_findEOF = false;
	} else
	{
		// Move caret to next line. Workaround, stuck on same word.
		line = m_editor->GetCurrentLine();
		m_editor->GotoLine( line+1 );
	}

	// Move Anchor to caret.
	m_editor->SearchAnchor();
	// Find pos
	int pos = m_editor->SearchNext( wxSTC_FIND_MATCHCASE, val );

	if( pos != -1 )
	{
		// TODO: Workaround.
		// Scroll to line
		line = m_editor->GetCurrentLine();
		m_editor->GotoLine( line );

		// Search again to Highlight.
		m_editor->SearchNext( wxSTC_FIND_MATCHCASE, val );
	} else
	{
		// Yet another workaround.
		m_findEOF = true;
	}
	m_editor->SetFocus();
}


// cut copy paste
void myFrame::OnCut( wxCommandEvent& event )
{
	// Editor checks for empty strings.
	m_editor->Cut();
}

void myFrame::OnCopy( wxCommandEvent& event )
{
	// Editor checks for empty strings.
	m_editor->Copy();
}

void myFrame::OnPaste( wxCommandEvent& event )
{
	m_editor->Paste();
}

void myFrame::OnMenuFind( wxCommandEvent& event )
{
	m_findText->SetFocus();
	m_findText->SetSelection(-1, -1);
}

void myFrame::OnMenuGoto( wxCommandEvent& event )
{
	m_gotoVal->SetFocus();
	m_gotoVal->SetSelection(-1, -1);
}

void myFrame::OnGotoChanged( wxSpinEvent& event )
{
	int val = m_gotoVal->GetValue();
	val--; // match 0 base
	m_editor->GotoLine( val );
	m_editor->SetFocus();
}

// AutoComplete
void myFrame::OnAC_Scan_Folder( wxCommandEvent& event )
{
	// debug
	cout << "AC Scan Folder Clicked..." << endl;

	wxDirDialog Dir( NULL, "Choose Folder to scan", "", wxDD_DEFAULT_STYLE );

	int result = Dir.ShowModal();
	if( result == wxID_OK )
	{
		scanFolder( Dir.GetPath().ToStdString() );
	}
}

void myFrame::OnAC_Clear_All( wxCommandEvent& event )
{
	// debug
	cout << "AC Scan Folder Clicked..." << endl;

	m_autocomplete->clearWordList();
}

// Context Menu
void myFrame::OnContextMenu( wxTreeListEvent& event )
{
	// debug
	cout << "TreeList Context Menu..." << endl;
	wxTreeListItem item = event.GetItem();

	myClientData *data = (myClientData *)m_TreeListCtrl->GetItemData( item );
	if( data )
	{
		// debug
		// cout << "Item: " << m_TreeListCtrl->GetItemText( item );
		// cout << "  Full Path: " << data->GetFullPath() << endl;
		
		// TODO: fix this once I find widget relitive to window pos.
		wxPoint move( -20, -80 );

		if( data->GetItemType() == myClientData::Folder )
		{
			// Show popupmenu at position
			wxMenu menu("");
			menu.Append( ID_TLCM_scan_folder, "&Scan Folder" );
			menu.Append( ID_TLCM_new_file, "&New File" );
			menu.Append( ID_TLCM_new_folder, "&New Folder" );
			menu.AppendSeparator();
			menu.Append( ID_TLCM_delete_folder, "&Delete Folder" );
			PopupMenu( &menu, wxGetMousePosition() + move );

		} else if( data->GetItemType() == myClientData::Root )
		{
			// Show popupmenu at position
			wxMenu menu("");
			menu.Append( ID_TLCM_scan_folder, "&Scan Folder" );
			menu.Append( ID_TLCM_refresh, "&Refresh Tree" );
			menu.Append( ID_TLCM_new_file, "&New File" );
			menu.Append( ID_TLCM_new_folder, "&New Folder" );
			PopupMenu( &menu, wxGetMousePosition() + move );

		} else
		{
			// Show popupmenu
			wxMenu menu("");
			menu.Append( ID_TLCM_open_file, "&Open File" );
			menu.AppendSeparator();
			menu.Append( ID_TLCM_delete_file, "&Delete File" );
			PopupMenu( &menu, wxGetMousePosition() + move );
		}
	}
}

void myFrame::OnTLCM_Refresh( wxCommandEvent& event )
{
	// debug
	cout << "TLCM Refresh Clicked..." << endl;

	// Clear TreeListctrl
	m_TreeListCtrl->DeleteAllItems();
	// Load Project Folders
	wxTreeListItem root = m_TreeListCtrl->GetRootItem();
	loadTree( m_projectDir, 0, root );
		m_TreeListCtrl->Expand( m_TreeListCtrl->GetFirstChild( root) );
}

void myFrame::OnTLCM_New_File( wxCommandEvent& event )
{
	// debug
	cout << "TLCM New File Clicked..." << endl;

	wxTreeListItem item = m_TreeListCtrl->GetSelection();
	if(! item.IsOk() ) return;

	myClientData *data = (myClientData *)m_TreeListCtrl->GetItemData( item );
	if( data )
	{
		// debug
		// cout << "Item: " << m_TreeListCtrl->GetItemText( item );
		// cout << "  Full Path: " << data->GetFullPath() << endl;

		if( data->GetItemType() == myClientData::Folder ||
			data->GetItemType() == myClientData::Root
		)
		{
			m_currentDir = data->GetFullPath();

			// wxString wxGetTextFromUser( const wxString &message, const wxString &caption=wxGetTextFromUserPromptStr, const wxString &default_value=wxEmptyString, wxWindow *parent=NULL, int x=wxDefaultCoord, int y=wxDefaultCoord, bool centre=true )
			wxString filename =  wxGetTextFromUser( "Filename", "Enter a new Filename", "", this );
			if( filename != "" )
			{
				ofstream outfile( m_currentDir + "/" + filename.ToStdString() );
				outfile << std::endl;
				outfile.close();

				// Add to tree_sizer
				wxTreeListItem new_node = m_TreeListCtrl->AppendItem( item, filename, myFrame::Icon_File, wxTreeListCtrl::NO_IMAGE, new myClientData( myClientData::TextFile, m_currentDir + "/" + filename.ToStdString() ) );
				m_TreeListCtrl->SetItemText( new_node, 1, "New File" );
			}
		}
	}
}


void myFrame::OnTLCM_New_Folder( wxCommandEvent& event )
{
	// debug
	cout << "TLCM New Folder Clicked..." << endl;

	wxTreeListItem item = m_TreeListCtrl->GetSelection();
	if(! item.IsOk() ) return;

	myClientData *data = (myClientData *)m_TreeListCtrl->GetItemData( item );
	if( data )
	{
		// debug
		// cout << "Item: " << m_TreeListCtrl->GetItemText( item );
		// cout << "  Full Path: " << data->GetFullPath() << endl;

		if( data->GetItemType() == myClientData::Folder ||
			data->GetItemType() == myClientData::Root
		)
		{
			m_currentDir = data->GetFullPath();

			// wxString wxGetTextFromUser( const wxString &message, const wxString &caption=wxGetTextFromUserPromptStr, const wxString &default_value=wxEmptyString, wxWindow *parent=NULL, int x=wxDefaultCoord, int y=wxDefaultCoord, bool centre=true )
			wxString foldername =  wxGetTextFromUser( "Folder name", "Enter a new folder name", "", this );
			if( foldername != "" )
			{
				bool val = fs::create_directory( m_currentDir + "/" + foldername.ToStdString() );
				if( val )
				{
					// Add to tree
					wxTreeListItem new_node = m_TreeListCtrl->AppendItem( item, foldername, myFrame::Icon_FolderClosed, myFrame::Icon_FolderOpened, new myClientData( myClientData::Folder, m_currentDir + "/" + foldername.ToStdString() ) );
					m_TreeListCtrl->SetItemText( new_node, 1, "Directory" );
				
				} else
				{
					cerr << "Could not create directory : " <<  m_currentDir + foldername.ToStdString() << endl;
				}
			}
		}
	}
}

void myFrame::OnTLCM_Open_File( wxCommandEvent& event )
{
	// debug
	cout << "TLCM Open File Clicked..." << endl;
	
	wxTreeListItem item = m_TreeListCtrl->GetSelection();
	if(! item.IsOk() ) return;
	
	myClientData *data = (myClientData *)m_TreeListCtrl->GetItemData( item );
	if( data )
	{
		// debug
		// cout << "Item: " << m_TreeListCtrl->GetItemText( item );
		// cout << "  Full Path: " << data->GetFullPath() << endl;
		
		if(    data->GetItemType() == myClientData::SourceCpp
			|| data->GetItemType() == myClientData::SourceHeader
			|| data->GetItemType() == myClientData::SourceC
			|| data->GetItemType() == myClientData::TextFile
			|| data->GetItemType() == myClientData::Git
			|| data->GetItemType() == myClientData::Markdown
			|| data->GetItemType() == myClientData::XPM
			)
		{
			// auto save current file first
			saveCurrentFile();
			
			// load our file
			m_currentFile = data->GetFullPath();
			m_editor->LoadFile( m_currentFile );
			
			// move to stored carret pos
			m_editor->SetFocus();
			m_editor->GotoPos( data->GetCarretPos() );
			// debug
    		// cout << "Goto carret Pos : " << data->GetCarretPos() << endl;
			
			// Status bar
			SetStatusText( "Curent File : " + m_currentFile, 0 );
		} else
		{
			// dir sotoggle expand
			if( m_TreeListCtrl->IsExpanded( item ) )
			{
				m_TreeListCtrl->Collapse( item );
			} else
			{
				m_TreeListCtrl->Expand( item );
			}
		}
	}
}

void myFrame::OnTLCM_Delete_File( wxCommandEvent& event )
{
	// debug
	cout << "TLCM Delete File Clicked..." << endl;

	wxTreeListItem item = m_TreeListCtrl->GetSelection();
	if(! item.IsOk() ) return;

	myClientData *data = (myClientData *)m_TreeListCtrl->GetItemData( item );
	if( data )
	{
		// debug
		// cout << "Item: " << m_TreeListCtrl->GetItemText( item );
		// cout << "  Full Path: " << data->GetFullPath() << endl;

		if(! fs::remove( data->GetFullPath() ) )
		{
			cerr << "Could not delete : " << data->GetFullPath() << endl;
		} else
		{
			m_TreeListCtrl->DeleteItem( item );
		}
	}
}

void myFrame::OnTLCM_Delete_Folder( wxCommandEvent& event )
{
	// debug
	cout << "TLCM Delete Folder Clicked..." << endl;

	// ::remove_all("myDirectory");

	wxTreeListItem item = m_TreeListCtrl->GetSelection();
	if(! item.IsOk() ) return;

	myClientData *data = (myClientData *)m_TreeListCtrl->GetItemData( item );
	if( data )
	{
		// debug
		// cout << "Item: " << m_TreeListCtrl->GetItemText( item );
		// cout << "  Full Path: " << data->GetFullPath() << endl;

		if(! fs::remove_all( data->GetFullPath() ) )
		{
			cerr << "Could not delete folder : " << data->GetFullPath() << endl;
		} else
		{
			m_TreeListCtrl->DeleteItem( item );
		}
	}
}

void myFrame::OnTLCM_Scan_Folder( wxCommandEvent& event )
{
	// debug
	cout << "TLCM Scan Folder Clicked..." << endl;

	wxTreeListItem item = m_TreeListCtrl->GetSelection();
	if(! item.IsOk() ) return;

	myClientData *data = (myClientData *)m_TreeListCtrl->GetItemData( item );
	if( data )
	{
		// debug
		// cout << "Item: " << m_TreeListCtrl->GetItemText( item );
		// cout << "  Full Path: " << data->GetFullPath() << endl;

		scanFolder( data->GetFullPath() );
	}
}


