#include "lsAutoComplete.h"

lsAutoComplete::lsAutoComplete()
{
	m_word_list = new lsTrie();
	if(! m_word_list )
	{
		cerr << "ERROR: lsAutoComplete - failed to create lsTrie instance" << endl;
	}
}

lsAutoComplete::~lsAutoComplete()
{
	if( m_word_list ) delete m_word_list;
}

string lsAutoComplete::getWords( const string& prefix )
{
	vector<string> list;
	string str;

	list = m_word_list->findPrefix( prefix );
	if(! list.empty() )
	{
		for( auto word : list )
		{
			str = str + word + " ";
		}
		// remove trailing space.
		str.pop_back();
	}
	
	return str;
}

void lsAutoComplete::buildWordList( const string& dir )
{
	if(! m_word_list ) return;
	if( dir.empty() ) return;
	
	cout << "Build auto complete for : " << dir << endl;
	
	// clear the trie nodes
	m_word_list->clearTree();
	// recursive dir scan for c++ files.
	// seperate out words and add to Tree.
	m_word_list->appendTree( dir );
}


void lsAutoComplete::appendWordList( const string& dir )
{
	if(! m_word_list ) return;
	if( dir.empty() ) return;
	
	// recursive dir scan for c++ files.
	// seperate out words and add to Tree.
	m_word_list->appendTree( dir );
}

void lsAutoComplete::clearWordList()
{
	if(! m_word_list ) return;
	m_word_list->clearTree();
}

