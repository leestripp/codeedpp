#include <filesystem>

#include "lsTrie.h"

lsTrie::lsTrie()
{
	root = NULL;
}

void lsTrie::insert( const string& word )
{
	// debug
	// cout << "Insert Word: " << word << endl;
	
	if( word.length() < 2 ) return;
	if(! root) root = new trieNode();
	
	trieNode *tmp = root;
	for( ulong i=0; i<word.length(); i++ )
	{
		if( tmp->node.find( word[i] ) == tmp->node.end() )
		{
			// No letter found, so store it and add new Node.
			tmp->node[ word[i] ] = new trieNode();
		}
		// next node.
		tmp = tmp->node[ word[i] ];
    }
    // whole word added
	tmp->isWord = true;
}

void lsTrie::iterate_( const trieNode* trienode, const std::string& prefix )
{
	if( trienode->isWord )
	{
		// debug
		// cout << prefix << endl;
		
		m_wordlist.push_back( prefix );
	}
	
	for( const auto& [c, child] : trienode->node )
	{
		iterate_( child, prefix + c );
	}
}

void lsTrie::iterate()
{
	if( root )
	{
		// debug
		// cout << "Display all Words" << endl;
		
		m_wordlist.clear();
		iterate_( root, "" );
	} else
	{
		cout << "Tree empty..." << endl;
	}
}

void lsTrie::findPrefix_( const trieNode* node, const string& prefix, ulong pos )
{
	trieNode *tmp;
	
	if( auto search = node->node.find( prefix[pos] ); search != node->node.end() )
	{
		tmp = search->second;
		
		if( prefix.size() == pos+1 )
		{
			found = tmp;
			return;
		} else
		{
			pos++;
			findPrefix_( tmp, prefix, pos );
		}
	}
}

vector<string> lsTrie::findPrefix( const string& prefix )
{
	found = NULL;
	ulong pos = 0;
	
	if( root )
	{
		// debug
		// cout << "Find words with prefix: " << prefix << endl;
		findPrefix_( root, prefix, pos );
		
		// display words from node found.
		if( found )
		{
			// clear word list
			m_wordlist.clear();
			// Add new words
			iterate_( found, prefix );
		}
	} else
	{
		// Clear word list
		m_wordlist.clear();
	}
	
	return m_wordlist;
}

void lsTrie::clear_( const trieNode* trienode )
{
	for( const auto& [c, child] : trienode->node )
	{
		// debug
		// cout << " " << c;
		clear_( child );
		delete child;
	}
}

void lsTrie::clearTree()
{
	if( root )
	{
		// debug
		// cout << "clearNodes:";
		clear_( root );
		delete root;
		// cout << endl;
	}
	root = NULL;
}

vector<string> lsTrie::regex_split( const string &s, const regex& sep_regex )
{
	sregex_token_iterator iter( s.begin(), s.end(), sep_regex, -1 );
	sregex_token_iterator end;

	return {iter, end};
}


string lsTrie::loadFile( const string& file )
{
	stringstream ss;
	
	ifstream myfile( file );
	if( myfile.is_open() )
	{
		ss << myfile.rdbuf() << endl;
	}
	return ss.str();
}

string lsTrie::removeComments( const string& input )
{
	bool record = true;
	bool singleline = false;
	string output, output2, buffer;
	
	// single line comments.
	for( ulong i=0; i < input.length(); i++)  
	{
		buffer = buffer + input[i];
		
		if(( buffer == "//" )&&(! singleline  ))
		{
			singleline = true;
			record = false;
			output.pop_back();
		}
		
		if( singleline )
		{
			if( ( input[i] == '\n' )||( input[i] == '\r' ) )
			{
				singleline = false;
				record = true;
			}
		}
		
		// buffer slide
		if( buffer.length() > 1 )
		{
			buffer[0] = buffer[1];
			buffer.pop_back();
		}
		
		if( record )
		{
			output = output + input[i];
		}
	} // for
	
	// multiline comments.
	buffer = "";
	for( ulong i=0; i < output.length(); i++)  
	{
		buffer = buffer + output[i];
		
		if( buffer == "/*" )
		{
			record = false;
		}
		
		if( buffer == "*/" )
		{
			record = true;
		}
		
		// buffer slide
		if( buffer.length() > 1 )
		{
			buffer[0] = buffer[1];
			buffer.pop_back();
		}
		
		if( record )
		{
			output2 = output2 + output[i];
		}
	}
	
	return output2;
}


//*******************
// create

void lsTrie::appendTree( const string& dir )
{
	for( const auto& p: filesystem::recursive_directory_iterator( dir ) )
	{
		if(! filesystem::is_directory(p) )
		{
			// skip files in build, .git etc...
			string s = p.path();
			
			auto index = s.find( "build" );
			if( index != string::npos ) continue;
			index = s.find( ".git" );
			if( index != string::npos ) continue;
			
			string filename, path, ext;
			
			ext = p.path().extension();
			filename = p.path().filename();
			path = p.path();
			
			if(( ext == ".cpp" )||( ext == ".h" )||( filename == "CMakeLists.txt" ))
			{
				// debug
				// cout << "Ext: " << ext << "  Path: " << p.path() << endl;
				
				// load file
				string contents;
				cout << "Processing file: " << path << endl;
				contents = loadFile( path );
				contents = removeComments( contents );
				vector<string> list;
				list = regex_split( contents );
				for( auto word: list )
				{
					// Skip numbers etc...
					if( checkNumber( word[0] ) ) continue;
					
					insert( word );
					
				}
			}
		}
	}
}

bool lsTrie::checkNumber( const char& ch )
{
	bool result = false;
	
	switch( ch )
	{
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			result = true;
			break;
	}
	
	return result;
}

