#ifndef LSAUTOCOMPLETE_H
#define LSAUTOCOMPLETE_H

#include <iostream>

#include "lsTrie.h"

using namespace std;


class lsAutoComplete
{
public:
	lsAutoComplete();
	virtual ~lsAutoComplete();
	
	string getWords( const string& prefix );
	
	void buildWordList( const string& dir );
	void appendWordList( const string& dir );
	void clearWordList();
	
private:
	lsTrie *m_word_list;
};

#endif // LSAUTOCOMPLETE_H
