#ifndef LSTRIE_H
#define LSTRIE_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <unordered_map>
#include <regex>

using namespace std;

// Data
class trieNode
{
public:
	unordered_map<char, trieNode*> node;
	bool isWord;
};

// Base Manager
class lsTrie
{
public:
	lsTrie();
	
	// trie tree
	void insert( const string& word );
	void iterate();
	void clearTree();
	vector<string> findPrefix( const string& prefix );
	// create
	void appendTree( const string& dir );
	
private:
	void iterate_( const trieNode* node, const string& prefix );
	void clear_( const trieNode* trienode );
	void findPrefix_( const trieNode* node, const string& prefix, ulong pos );
	// Strings
	string removeComments( const string& input );
	vector<string> regex_split( const string &s, const regex& sep_regex = regex{"[\\s.,&?!\\(\\);\\{\\}\"\t=\\-\\+#|$\\*\\[\\]/<>]+"} );
	// Char
	bool checkNumber( const char& ch );
	//files
	string loadFile( const string& file );
	
	trieNode *root;
	trieNode *found;
	vector<string> m_wordlist;
};

#endif // LSTRIE_H

