#ifndef MYFRAME_H
#define MYFRAME_H

#include <iostream>

#include <wx/wx.h>
#include <wx/treelist.h>
#include <wx/stc/stc.h>
#include <wx/spinctrl.h>

// event EDs
enum
{
	ID_Open_Project = wxID_HIGHEST + 1,
	ID_New_Project,
	ID_File_Save,
	ID_File_Close,
	ID_cut,
	ID_copy,
	ID_paste,
	ID_menu_find,
	ID_find_next,
	ID_find_replace,
	ID_findtext_enter,
	ID_menu_goto,
	ID_goto_changed,
	ID_AC_scan_folder,
	ID_AC_clear_all,

	ID_TreeList,
	ID_TLCM_refresh,
	ID_TLCM_open_file,
	ID_TLCM_new_file,
	ID_TLCM_new_folder,
	ID_TLCM_delete_file,
	ID_TLCM_delete_folder,
	ID_TLCM_scan_folder,

	ID_stc_editor,
	ID_find_button,
	ID_goto_button
};

// StyleTextCtrl
// Margins
enum
{
	MARGIN_LINE_NUMBERS,
	MARGIN_FOLD
};

// protos
class wxStyledTextCtrl;
class wxPanel;
class lsAutoComplete;


class myFrame : public wxFrame
{
public:
    myFrame( const wxString& title, const wxPoint& pos, const wxSize& size );
	virtual ~myFrame();
	
	// TreeList Icons
    enum
    {
        Icon_File,
        Icon_FolderClosed,
        Icon_FolderOpened
    };

private:
	void InitImageList();
	// Menus
	void OnOpenProject( wxCommandEvent& event );
	void OnNewProject( wxCommandEvent& event );
	void OnFileSave( wxCommandEvent& event );
	void OnFileClose( wxCommandEvent& event );
    void OnExit( wxCommandEvent& event );
	// cut copy paste
	void OnCut( wxCommandEvent& event );
	void OnCopy( wxCommandEvent& event );
	void OnPaste( wxCommandEvent& event );
	void OnMenuFind( wxCommandEvent& event );
	void OnMenuGoto( wxCommandEvent& event );
	void OnGotoChanged( wxSpinEvent& event );
	// AutoComplete
	void OnAC_Scan_Folder( wxCommandEvent& event );
	void OnAC_Clear_All( wxCommandEvent& event );

	// TLCM Context Menu
	void OnContextMenu( wxTreeListEvent& event );
	void OnTLCM_Refresh( wxCommandEvent& event );
	void OnTLCM_New_File( wxCommandEvent& event );
	void OnTLCM_New_Folder( wxCommandEvent& event );
	void OnTLCM_Open_File( wxCommandEvent& event );
	void OnTLCM_Delete_File( wxCommandEvent& event );
	void OnTLCM_Delete_Folder( wxCommandEvent& event );
	void OnTLCM_Scan_Folder( wxCommandEvent& event );

    void onTreeItemActivated( wxTreeListEvent& event );
    void OnMarginClick( wxStyledTextEvent &event );
    void OnTextModified( wxStyledTextEvent& event );
	void OnEditorCharAdded( wxStyledTextEvent& event );
	void OnGotoLine( wxCommandEvent& event );
	void OnFindText( wxCommandEvent& event );

	// project
	void loadTree( const std::string &path, int lvl, wxTreeListItem node );
	void saveCurrentFile();
	// auto complete
	void scanFolder( const std::string &path );		// Append scan
	// TreeList
	wxTreeListItem findTreeItem( const std::string& s );

	// UI
	wxImageList *m_imageList;
	bool setup_Editor();
	wxStyledTextCtrl *m_editor;
	// TreeList
	wxTreeListCtrl *m_TreeListCtrl;
	wxTextCtrl *m_findText;
	wxSpinCtrl *m_gotoVal;

	wxPanel *m_panel_left;
	wxPanel *m_panel_right;
	
	// filesys
    wxString m_user_home;
	// current opened file
	std::string m_projectDir;			// full path
	std::string m_currentFile;			// full path
	std::string m_currentDir;			// full path
	
	// editor
	int m_currentPos;					// current cursor (carret) position.
	bool m_findEOF;

	// autocomplete
	lsAutoComplete *m_autocomplete;
	std::string m_prefix;
	
    wxDECLARE_EVENT_TABLE();
};

#endif // MYFRAME_H

