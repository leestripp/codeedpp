#ifndef MYCLIENTDATA_H
#define MYCLIENTDATA_H

#include <iostream>
#include <wx/clntdata.h>


class myClientData : public wxClientData
{
public:
    enum ItemType
    {
        Root,
        TextFile,
        SourceCpp,
        SourceHeader,
        SourceC,
        Markdown,
        Image,
        XPM,
        Git,
        Folder,
        Unknown
    };
	
    myClientData( ItemType val, const std::string &full_path );

    ItemType GetItemType() const{return m_itemType;}
	const std::string& GetFullPath() {return m_fullpath;}
	
	void StoreCarretPos( int val ) {m_carretpos = val;}
	int GetCarretPos() {return m_carretpos;}
private:
    ItemType m_itemType;
    std::string m_fullpath;
    
    int m_carretpos;
};

#endif // MYCLIENTDATA_H
